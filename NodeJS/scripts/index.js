"use strict";
window.onload = init;
function init() {

    const urlParams = new URLSearchParams(location.search);

    let id = -1;
    if (urlParams.has("courseid") === true || urlParams.has("id") === true) {
        id = urlParams.get("courseid") || urlParams.get("id");
        getCourseDetail(id);
    }

    const getToDoBtn = document.getElementById("getToDoBtn");
    const getUsersBtn = document.getElementById("getUsersBtn");
    const getCoursesBtn = document.getElementById("getCoursesBtn");
    const submitCourse = document.getElementById("myCourse");
    const confirmDeleteBtn = document.getElementById("confirmDeleteBtn");

    if (getToDoBtn) {
        getToDoBtn.onclick = onGetToDoBtnClicked;
    }
    if (getUsersBtn) {
        getUsersBtn.onclick = onGetUsersBtnClicked;
    }
    if (getCoursesBtn) {
        getCoursesBtn.onclick = onGetCoursesBtnClicked;
    }
    if (submitCourse) {
        submitCourse.onsubmit = onSubmitCourse;
    }
    if (confirmDeleteBtn) {
        confirmDeleteBtn.onclick = btnDeleteClicked;
    }

    if (window.location.href.includes("index.html") || window.location.pathname == '/NodeJS/') {
        onGetCoursesBtnClicked();
    }

}

function onSubmitCourse() {

    // let data = new FormData(document.getElementById("myCourse"));
    // let noFormData = document.getElementById("myCourse");
    // console.log(noFormData);
    // console.log(noFormData.serial);
    // console.log(document.getElementById("myCourse"));

    // console.log(...data);
    // console.log(data);
    // for (let [name, value] of data) {
    //     console.log(`${name} = ${value}`); // key1 = value1, then key2 = value2
    // }
    // var object = {};
    // data.forEach((value, key) => object[key] = value);

    let formData = new FormData(document.getElementById("myCourse"));
    let formDataObject = Object.fromEntries(formData.entries());
    var json = JSON.stringify(formDataObject);

    console.log(json);

    let url = "http://localhost:8081/api/courses/";
    fetch(url, {
        method: "POST",
        body: json,
        headers: {
            "Content-type": "application/json",
            "Access-Control-Allow-Origin": "*",
        },
    })
        .then((response) => {
            if (response.status >= 200 && response.status < 300) {
                console.log(response.json());
                window.location.href = "index.html";
                return false;
            } else {
                throw new Error();
            }
        })
        .then((json) => {
            console.dir(json);
        })
        .catch((err) => console.log(err));
    return false;
}

function btnDeleteClicked() {

    console.log("btnDeleteClicked");
    const urlParams = new URLSearchParams(location.search);
    let toDoField = document.getElementById("toDoField");
    let id = -1;
    if (urlParams.has("courseid") === true || urlParams.has("id") === true) {
        id = urlParams.get("courseid") || urlParams.get("id");

    }
    if (id && id > 0) {
        // urlParams.has("id")
        // urlParams.
        // send DELETE request w/ id as part of URL
        fetch("http://localhost:8081/api/courses/" + id, {
            method: "DELETE"
        })
            .then((response) => {
                if (response.status == 200) {
                    console.log(response.json());
                    window.location.href = "index.html";
                    return false;
                } else {
                    throw new Error();
                }
            })
            .catch(err => {
                console.log(err);
                // If the DELETE returns an error, display a message
                let confirmationMessage =
                    document.getElementById("confirmationMessage");
                confirmationMessage.innerHTML = "Unexpected error.";
            });
    }
}











function onGetToDoBtnClicked() {
    let toDoField = document.getElementById("toDoField");
    if (toDoField.value > 0 && toDoField.value < 201) {
        let element = document.getElementById("messageArea");
        fetch("http://jsonplaceholder.typicode.com/todos/" + toDoField.value)
            .then(response => response.json())
            .then(data => {
                let message = "ToDo - Title: " + data.title;
                //element.innerHTML = message;
                buildToDoTable(data);
            });
    }
}

function buildToDoTable(data) {
    // Create an empty <tr> element and add it to the last
    // position of the table
    let toDoTblBody = document.getElementById("toDoTblBody");
    let toDoTbl = document.getElementById("toDoTbl");
    toDoTblBody.innerHTML = "";
    toDoTbl.style.display = 'block';

    console.log(data);
    //{userId: 1, id: 2, title: 'quis ut nam facilis et officia qui', completed: false}
    const labels = {
        userId: 'User ID',
        id: 'ToDo ID',
        title: 'Title',
        completed: 'Completed'
    };

    const replacedKeysInData = {}
    Object.keys(data).forEach((key) => {
        const keyFromMap = labels[key] || key;
        replacedKeysInData[keyFromMap] = data[key]
    })

    data = replacedKeysInData;

    console.log(data);
    // {User ID: 1, ToDo ID: 2, Title: 'quis ut nam facilis et officia qui', Completed: false}

    let renameKeys = (keysMap, object) =>
        Object.keys(object).reduce(
            (acc, key) => ({
                ...acc,
                ...{ [keysMap[key] || key]: object[key] },
            }),
            {}
        );

    let result = renameKeys(labels,
        data);
    console.log(result);
    //{User ID: 1, ToDo ID: 2, Title: 'quis ut nam facilis et officia qui', Completed: false}

    for (const property in data) {
        //console.log(`${property}: ${data[property]}`);
        let value = data[property];
        let rowName = property;
        if (property == "projectsAssignedTo") {
            value = data[property].length;
            rowName = property;
            //continue;
        }

        let row = toDoTblBody.insertRow(-1);
        // Create new cells (<td> elements) and add text
        let cell1 = row.insertCell(0);
        cell1.innerHTML = rowName;
        let cell2 = row.insertCell(1);
        cell2.innerHTML = value;

    }

}

function getCourseDetail(courseId) {
    //let toDoField = document.getElementById("toDoField");
    if (courseId > 0) {
        //let element = document.getElementById("messageArea");
        //console.log(courseId);
        fetch("http://localhost:8081/api/courses/" + courseId)
            .then(response => response.json())
            .then(data => {
                //let message = "ToDo - Title: " + data.title;
                //element.innerHTML = message;
                //console.log(data);
                buildCourseDetailTable(data);
            });
    }
}

function buildCourseDetailTable(data) {
    // Create an empty <tr> element and add it to the last
    // position of the table
    let courseDetailTblBody = document.getElementById("courseDetailTblBody");
    let courseDetailTbl = document.getElementById("courseDetailTbl");
    courseDetailTblBody.innerHTML = "";
    courseDetailTbl.style.display = 'block';

    // console.log(data);
    //{userId: 1, id: 2, title: 'quis ut nam facilis et officia qui', completed: false}
    const labels = {
        courseNum: 'Course Num',
        id: 'Course ID',
        courseName: 'Course Name',
        instructor: 'Instructor',
        dept: 'Department',

        startDate: 'Start Date',
        numDays: 'Number of Days'
    };

    const replacedKeysInData = {}
    Object.keys(data).forEach((key) => {
        const keyFromMap = labels[key] || key;
        replacedKeysInData[keyFromMap] = data[key]
    })

    data = replacedKeysInData;

    // console.log(data);
    // {User ID: 1, ToDo ID: 2, Title: 'quis ut nam facilis et officia qui', Completed: false}

    let renameKeys = (keysMap, object) =>
        Object.keys(object).reduce(
            (acc, key) => ({
                ...acc,
                ...{ [keysMap[key] || key]: object[key] },
            }),
            {}
        );

    let result = renameKeys(labels,
        data);
    // console.log(result);
    //{User ID: 1, ToDo ID: 2, Title: 'quis ut nam facilis et officia qui', Completed: false}

    for (const property in data) {
        //console.log(`${property}: ${data[property]}`);
        let value = data[property];
        let rowName = property;
        if (property == "projectsAssignedTo") {
            value = data[property].length;
            rowName = property;
            //continue;
        }

        let row = courseDetailTblBody.insertRow(-1);
        // Create new cells (<td> elements) and add text
        let cell1 = row.insertCell(0);
        cell1.innerHTML = rowName;
        let cell2 = row.insertCell(1);
        cell2.innerHTML = value;

    }

}

function onGetUsersBtnClicked() {

    let table = document.getElementById("usersTbl");
    let tableBody = document.getElementById("usersTblBody");
    tableBody.innerHTML = "";
    table.style.display = 'block';
    fetch("http://jsonplaceholder.typicode.com/users")
        .then(response => response.json())
        .then(data => {
            for (let i = 0; i < data.length; i++) {
                let row = tableBody.insertRow(-1);
                let cell1 = row.insertCell(0);
                let cell2 = row.insertCell(1);
                let cell3 = row.insertCell(2);
                let cell4 = row.insertCell(3);
                let cell5 = row.insertCell(4);
                let cell6 = row.insertCell(5);
                cell1.innerHTML = data[i].id;
                cell2.innerHTML = data[i].name;
                cell3.innerHTML = data[i].email;
                cell4.innerHTML = data[i].username;
                cell5.innerHTML = data[i].phone;
                cell6.innerHTML = data[i].website;

            }
        });
}

function onGetCoursesBtnClicked() {

    // {
    //     "id": 1,
    //     "dept": "CompSci",
    //     "courseNum": 101,
    //     "courseName": "HTML5 and CSS3",
    //     "instructor": "Rob",
    //     "startDate": "July 8",
    //     "numDays": 10
    //  },

    let table = document.getElementById("coursesTbl");
    let tableBody = document.getElementById("coursesTblBody");
    tableBody.innerHTML = "";
    table.style.display = 'block';
    fetch("http://localhost:8081/api/courses")
        .then(response => response.json())
        .then(data => {
            for (let i = 0; i < data.length; i++) {
                let row = tableBody.insertRow(-1);
                let cell1 = row.insertCell(0);
                let cell2 = row.insertCell(1);
                let cell3 = row.insertCell(2);
                let cell4 = row.insertCell(3);
                cell1.innerHTML = data[i].courseNum;
                cell2.innerHTML = data[i].dept;
                cell3.innerHTML = "<a href=\"details.html?courseid=" + data[i].id + "\">" + data[i].courseName + "</a>";
                cell4.innerHTML = "<a href=\"confirm-delete.html?id=" + data[i].id + "\">" + "Delete Course" + "</a>";



            }
        });
}

function goToNewCourses() {
    window.location.href = "new-courses.html";
}

//Function demonstrating the Replace Method

function goToNewCoursesTwo() {
    window.location.replace("new-courses.html");
}