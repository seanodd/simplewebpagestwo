"use strict";

let winningTickets = [
    { tixNum: "1001001", expires: "2022-09-05", prize: 100000 },
    { tixNum: "1298711", expires: "2022-10-10", prize: 250000 },
    { tixNum: "1309182", expires: "2022-12-30", prize: 500000 },
    { tixNum: "1456171", expires: "2023-01-20", prize: 1000000 },
    { tixNum: "3332871", expires: "2022-05-20", prize: 1000000 },
    { tixNum: "4651529", expires: "2022-12-15", prize: 100000 },
    { tixNum: "5019181", expires: "2023-01-31", prize: 250000 },
    { tixNum: "5168261", expires: "2023-03-01", prize: 1000000 },
    { tixNum: "6761529", expires: "2022-12-15", prize: 250000 },
    { tixNum: "7778172", expires: "2023-01-15", prize: 5000000 },
    { tixNum: "8751426", expires: "2020-09-15", prize: 100000 }
];

window.onload = init;
function init() {

    loadWinningTicketsTable();


    const imageList = document.getElementById("imageList");
    const addImgButton = document.getElementById("addImgButton");
    const clearImgButton = document.getElementById("clearImgButton");
    addImgButton.onclick = onAddImgButtonClick;
    clearImgButton.onclick = onClearImgButtonClick;

    let imageFiles = [
        { name: "images/ziggy_landscape.jpg", description: "ziggy landscape", code: "ziggy_landscape" },
        { name: "images/orangie.jpg", description: "orangie", code: "orangie" },
        { name: "images/two_cats.jpg", description: "two cats", code: "two_cats" },
        { name: "images/ziggy_sleeping.jpg", description: "ziggy sleeping", code: "ziggy_sleeping" },
        { name: "images/orangie_sleeping.jpg", description: "orangie sleeping", code: "orangie_sleeping" }
    ];

    let selectHeader = new Option("Select a image", "");

    imageList.appendChild(selectHeader);
    for (let i = 0; i < imageFiles.length; i++) {
        let theOption = new Option(imageFiles[i].description, imageFiles[i].code);
        imageList.appendChild(theOption);
    }

    // for (let i = 0; i < imageFiles.length; i++) {

    //     let img = document.createElement("img");
    //     img.src = imageFiles[i].name;
    //     img.alt = imageFiles[i].description;
    //     const locationDiv = document.getElementById("imageWrap");
    //     locationDiv.appendChild(img);
    // }

}

// function loadWinningTicketsTable() {

// }

// function buildTicketRow(tbody, theTicket) {
// }

function loadWinningTicketsTable() {
    // Find the table
    let tbody = document.getElementById("winningTicketsTbl");
    // loop through the array
    let numWinningTickets = winningTickets.length;
    for (let i = 0; i < numWinningTickets; i++) {
      buildTicketRow(tbody, winningTickets[i]);
    }
  
    //  alert(table.rows.length);
  }
  
  function buildTicketRow(tbody, ticket) {
    // Create an empty <tr> element and add it to the last
    // position of the table
    let row = tbody.insertRow(-1);
    // Create new cells (<td> elements) and add text
    let cell1 = row.insertCell(0);
    cell1.innerHTML = ticket.tixNum;
    let cell2 = row.insertCell(1);
    cell2.innerHTML = "$" + ticket.prize.toFixed(2);
    let cell3 = row.insertCell(2);
    cell3.innerHTML = ticket.expires;
  }
  


function onAddImgButtonClick() {

    if (imageList.selectedIndex > 0) {
        //console.log("imageList.selectedIndex: " + imageList.selectedIndex);
        let text =
            imageList.options[imageList.selectedIndex].text;
        let value = imageList.value;
        //console.log("Selected: " + text + "\nValue: " + value);
        let img = document.createElement("img");
        img.src = "images/" + value + ".jpg";
        img.alt = text;
        const locationDiv = document.getElementById("imageWrap");
        locationDiv.appendChild(img);

    }

}
function onClearImgButtonClick() {
    const locationDiv = document.getElementById("imageWrap");
    locationDiv.innerHTML = "";
}
function display(item) {
    console.log(item);
}