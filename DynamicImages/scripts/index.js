"use strict";
window.onload = init;
function init() {

    const imageList = document.getElementById("imageList");
    const addImgButton = document.getElementById("addImgButton");
    const clearImgButton = document.getElementById("clearImgButton");
    addImgButton.onclick = onAddImgButtonClick;
    clearImgButton.onclick = onClearImgButtonClick;

    let imageFiles = [
        { name: "images/ziggy_landscape.jpg", description: "ziggy landscape", code: "ziggy_landscape" },
        { name: "images/orangie.jpg", description: "orangie", code: "orangie" },
        { name: "images/two_cats.jpg", description: "two cats", code: "two_cats" },
        { name: "images/ziggy_sleeping.jpg", description: "ziggy sleeping", code: "ziggy_sleeping" },
        { name: "images/orangie_sleeping.jpg", description: "orangie sleeping", code: "orangie_sleeping" }
    ];

    let selectHeader = new Option("Select a image", "");

    imageList.appendChild(selectHeader);
    for (let i = 0; i < imageFiles.length; i++) {
        let theOption = new Option(imageFiles[i].description, imageFiles[i].code);
        imageList.appendChild(theOption);
    }

    // for (let i = 0; i < imageFiles.length; i++) {

    //     let img = document.createElement("img");
    //     img.src = imageFiles[i].name;
    //     img.alt = imageFiles[i].description;
    //     const locationDiv = document.getElementById("imageWrap");
    //     locationDiv.appendChild(img);
    // }

}

function onAddImgButtonClick() {

    if (imageList.selectedIndex > 0) {
        //console.log("imageList.selectedIndex: " + imageList.selectedIndex);
        let text =
            imageList.options[imageList.selectedIndex].text;
        let value = imageList.value;
        //console.log("Selected: " + text + "\nValue: " + value);
        let img = document.createElement("img");
        img.src = "images/" + value + ".jpg";
        img.alt = text;
        const locationDiv = document.getElementById("imageWrap");
        locationDiv.appendChild(img);

    }

}
function onClearImgButtonClick() {
    const locationDiv = document.getElementById("imageWrap");
    locationDiv.innerHTML = "";
}
function display(item) {
    console.log(item);
}