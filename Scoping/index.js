"use strict";
function test1() {
    let a1 = 10;
    if (a1 > 5) {
        a1 = 7;
    }
    console.log("a1 = " + a1);
}
test1();

console.log("---------------------");

function test2A() {
    if (1 == 1) {
        var a2 = 5;
    }
    console.log("a2 = " + a2);
}
test2A();

console.log("---------------------");

// function test2B() {
//     if (1 == 1) {
//         let a = 5;
//     }
//     console.log("a = " + a);
// }
// test2B();

console.log("---------------------");
let a4 = 4;
function test3() {
    a4 = 3;
    console.log("a4 = " + a4);
}
test3();
console.log("a4 = " + a4);

console.log("---------------------");

let b = 4;
function test4() {
    let b = 7;
    console.log("b = " + b);
}
test4();
console.log("b = " + b);

console.log("---------------------");
"use strict";
let a5 = 4;
function test5() {
    a5 = 7;
    function again() {
        let a5 = 8;
        console.log("a5 = " + a5);
    }
    again();
    console.log("a5 = " + a5);
}
test5();
console.log("a5 = " + a5);



console.log("---------------------");
"use strict";
let a6 = 4;
function test6() {
let a6 = 7;
function again() {
let a6 = 8;
console.log("a6 = " + a6);
}
again();
console.log("a6 = " + a6);
}
test6();
console.log("a6 = " + a6);