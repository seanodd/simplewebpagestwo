// let myName1 = "Sean Daudlin";

// parseAndDisplayName(myName1)
// parseAndDisplayName("Brenda Kaye");
// parseAndDisplayName("Ian Auston");
// parseAndDisplayName("Siddalee Grace");

parseAndDisplayName("Cher");
parseAndDisplayName("Brenda Kaye");
parseAndDisplayName("Dana Lynn Wyatt");

function parseAndDisplayName(full_name) {
    let pos = full_name.indexOf(" ");
    let pos2 = full_name.lastIndexOf(" ");
    let first = full_name.substring(0, pos); // "Betty"
    let last = full_name.substring(pos + 1);
    let mid;
    console.log("Name       : " + full_name);
    // console.log("pos        : " + pos);
    // console.log("pos2       : " + pos2);
    if (pos < 0) {
        console.log("Only name  : " + full_name);
    } else {
        console.log("First name : " + first);
        if (pos2 > 0 && pos2 != pos) {
            mid = full_name.substring(pos + 1, pos2 + 1);
            last = full_name.substring(pos2 + 1);
            console.log("Middle name: " + mid)
        }

        console.log("Last name  : " + last);
    }

}

