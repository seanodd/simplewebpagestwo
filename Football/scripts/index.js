"use strict";

window.onload = function () {

    initializeDropdown();

    const teamsList = document.getElementById("teamsList");
    const teamInfoButton = document.getElementById("teamInfoButton");
    const teamInfo = document.getElementById("teamInfo");
    const teamImage = document.getElementById("teamImage");

    teamsList.onchange = onTeamsListChanged;
    teamInfoButton.onclick = onTeamInfoButtonClick;
};

function initializeDropdown() {
    let selectHeader = new Option("Select a team", "");

    let teams = [
        { code: "DAL", name: "Dallas Cowboys", plays: "Arlington, TX" },
        { code: "DEN", name: "Denver Broncos", plays: "Denver, CO" },
        { code: "HOU", name: "Houston Texans", plays: "Houston, TX" },
        {
            code: "KAN", name: "Kansas City Chiefs", plays: "Kansas City, MO"
        },
    ];

    teamsList.appendChild(selectHeader);
    for (let i = 0; i < teams.length; i++) {
        let theOption = new Option(teams[i].name, teams[i].code);
        teamsList.appendChild(theOption);
    }
}

function onTeamsListChanged() {

    teamInfo.style.display = 'none';
    teamInfo.innerHTML = "";

    if (teamsList.selectedIndex > 0) {
        let text =
            teamsList.options[teamsList.selectedIndex].text;
        let value = teamsList.value;
        //console.log("Selected: " + text + "\nValue: " + value);
        teamInfoButton.style.display = 'block';

        teamImage.src = "images/" + teamsList.value.toLowerCase() + "_logo.png";
        teamImage.style.display = 'block';
    }
    else {
        teamInfoButton.style.display = 'none';
        teamImage.style.display = 'none';
    }
}

function onTeamInfoButtonClick() {
    let teams = [
        { code: "DAL", name: "Dallas Cowboys", plays: "Arlington, TX" },
        { code: "DEN", name: "Denver Broncos", plays: "Denver, CO" },
        { code: "HOU", name: "Houston Texans", plays: "Houston, TX" },
        {
            code: "KAN", name: "Kansas City Chiefs", plays: "Kansas City, MO"
        },
    ];

    if (teamsList.selectedIndex >= 0) {
        let text =
            teamsList.options[teamsList.selectedIndex].text;
        let value = teamsList.value;
        let location= teams[teamsList.selectedIndex-1].plays;
 
        //console.log("Selected: " + text + "\nValue: " + value);
        teamInfo.style.display = 'block';
        teamInfo.innerHTML = "You selected the " + text + " (" + value + ") who play in " + location + ".";

    }
}

// window.onload = init;
// function init() {
//     const totalCalcButton = document.getElementById("totalCalcButton");
//     let under25no = document.getElementById("under25no");
//     let under25yes = document.getElementById("under25yes");
//     let tollTag = document.getElementById("tollTag");
//     let gps = document.getElementById("gps");
//     let roadside = document.getElementById("roadside");
//     let numOfDaysField = document.getElementById("numOfDaysField");


//     numOfDaysField.onchange = onTotalCalcButtonButtonClicked;
//     totalCalcButton.onclick = onTotalCalcButtonButtonClicked;
//     under25no.onclick = onTotalCalcButtonButtonClicked;
//     under25yes.onclick = onTotalCalcButtonButtonClicked;
//     tollTag.onclick = onTotalCalcButtonButtonClicked;
//     gps.onclick = onTotalCalcButtonButtonClicked;
//     roadside.onclick = onTotalCalcButtonButtonClicked;

// }

// function onGreetBtnClicked() {
//     let nameField = document.getElementById("nameField");
//     alert("Hi there " + nameField.value + "!");
// }

// function onTotalCalcButtonButtonClicked() {

//     // let numOfDaysField = document.getElementById("numOfDaysField");

//     let carRentalField = document.getElementById("carRentalField");
//     let rentalCarTotal = 29.99 * numOfDaysField.value;

//     // <input type="radio" name="under25" id="under25no" value="no"> No<br>
//     // <input type="radio" name="under25" id="under25yes" value="yes"> Yes<br>
//     //let selectedOption = document.querySelector("input[name='under25']:checked");

//     //    console.log(selectedOption.value)
//     let basePremium = 0;
//     let totalDue = 0.00;
//     let under_25_surcharge;
//     let optionsTotal = 0.00;
//     if (under25no.checked) {
//         under_25_surcharge = 0.00;
//     }
//     else if (under25yes.checked) {
//         under_25_surcharge = rentalCarTotal * 0.3;
//     }
//     else { // it must be life!
//         under_25_surcharge = 0.00;
//     }
//     console.log(under_25_surcharge);


//     if (tollTag.checked) {
//         optionsTotal += numOfDaysField.value * 3.95;
//     }
//     if (gps.checked) {
//         optionsTotal += numOfDaysField.value * 4.95;
//     }
//     if (roadside.checked) {
//         optionsTotal += numOfDaysField.value * 2.95;
//     }


//     // let basePremium = 0;
//     // if (selectedOption.value == "auto") {
//     // basePremium = 175.00;
//     // }
//     // else if (selectedOption.value == "home") {
//     // basePremium = 395.00;
//     // }
//     // else {
//     // basePremium = 225.00;
//     // }

//     // let currCoffeeField = document.getElementById("currCoffeeField");
//     // let coffeeRemaining = (maxCoffeeField.value - currCoffeeField.value);
//     // let coffeeMessage = 'You can have ' + coffeeRemaining + ' more coffee cups today.';

//     // let favColor = document.getElementById("favColor");
//     // console.log(favColor.value);
//     under25Field.value = under_25_surcharge;
//     carRentalField.value = rentalCarTotal;
//     optionsField.value = optionsTotal;
//     totalDue = rentalCarTotal + under_25_surcharge + optionsTotal;
//     totalField.value = totalDue;
//     //console.log("coffeeRemaining: " + coffeeRemaining);
//     //coffeeFinalAnswer = document.getElementById("coffeeFinalAnswer");
//     //console.log(coffeeFinalAnswer);
//     //coffeeFinalAnswer.innerHTML =  coffeeMessage ;


//     // coffeeFinalAnswer.style.color = favColor.value;
//     // coffeeFinalAnswer.innerHTML = 'You can have ' + coffeeRemaining + ' more coffee cups today.';

// }