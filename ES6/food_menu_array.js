let menu = [
    [
        { item: "Sausage and Egg Biscuit", price: 3.69 },
        { item: "Bacon and Egg Biscuit", price: 3.49 },
        { item: "Ham and Egg Biscuit", price: 3.29 }
    ],
    [
        { item: "Hamberger and Fries", price: 5.69 },
        { item: "Cheeseburger and Fries", price: 5.49 },
        { item: "Hotdog and Fries", price: 5.29 },
        { item: "Corndog and Fries", price: 5.19 }
    ],
    [{ item: "Steak", price: 9.69 },
    { item: "Pasta", price: 9.49 },
    { item: "Fish", price: 9.29 },
    { item: "Risotto", price: 9.29 },
    { item: "Sushi", price: 9.29 },
    ]
];

function showMenu(mealId) {
    let menuHeading = [
        "Breakfast Menu", "Lunch Menu", "Dinner Menu"
    ];
    console.log(menuHeading[mealId]);
    let numItems = menu[mealId].length;

    menu[mealId].sort(function (a, b) {
        if (a.price < b.price) return -1;
        else if (a.price == b.price) return 0;
        else return 1;
    });

    for (let j = 0; j < numItems; j++) {
        console.log(menu[mealId][j].item + " | Price : " + menu[mealId][j].price);
    }
}

showMenu(1);
