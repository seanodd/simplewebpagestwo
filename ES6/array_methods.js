let cart = [
    { item: "Bread", price: 3.29, quantity: 2 },
    { item: "Milk", price: 4.09, quantity: 1 },
    { item: "T-Bone Steak", price: 12.99, quantity: 2 },
    { item: "Baking Potato", price: 1.89, quantity: 6 },
    { item: "Iceberg Lettuce", price: 2.06, quantity: 1 },
    { item: "Ice Cream - Vanilla", price: 6.81, quantity: 1 },
    { item: "Apples", price: 0.66, quantity: 6 }
];


function buildItem(arrayElement) {
    return arrayElement.item;
}
function displayName(arrayElement) {
    console.log(arrayElement);
}
let cartList = cart.map(buildItem);
cartList.forEach(displayName);

function getTotalCost(currentTotal, arrayElement) {
    return currentTotal +
        (arrayElement.price * arrayElement.quantity);
}

console.log("-----------------------------------");
let sum = cart.reduce(getTotalCost, 0);
console.log("Sum of Cart: " + sum);
// sum contains 299.94


console.log("-----------------------------------");

cartList = cart.map(buildItem);
let sortedCartList = cartList.sort();
cartList.forEach(displayName);

console.log("-----------------------------------");