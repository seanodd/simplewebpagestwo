let courses = [
    {
    CourseId: "PROG100",
    Title: "Introduction to HTML/CSS/Git",
    Location: "Classroom 7",
    StartDate: "09/08/22",
    Fee: "100.00",
    },
    {
    CourseId: "PROG200",
    Title: "Introduction to JavaScript",
    Location: "Classroom 9",
    StartDate: "11/22/22",
    Fee: "350.00",
    },
    {
    CourseId: "PROG300",
    Title: "Introduction to Java",
    Location: "Classroom 1",
    StartDate: "01/09/23",
    Fee: "50.00",
    },
    {
    CourseId: "PROG400",
    Title: "Introduction to SQL and Databases",
    Location: "Classroom 7",
    StartDate: "03/16/23",
    Fee: "50.00",
    },
    {
    CourseId: "PROJ500",
    Title: "Introduction to Angular",
    Location: "Classroom 1",
    StartDate: "04/25/23",
    Fee: "50.00",
    }
   ];


// When does the PROG200 course start?
function findByCourseId(arrayValue) {
    if (arrayValue.CourseId ==  "PROG200") {
    return true;
    }
    else {
    return false;
    }
}
let arrayByCourseId = courses.find(findByCourseId);
let courseStartByCourseId = courses.find(findByCourseId).StartDate;

console.log(arrayByCourseId);
console.log(courseStartByCourseId);

// What is the title of the PROJ500 course?
function findByCoursePROJ500(arrayValue) {
    if (arrayValue.CourseId ==  "PROJ500") {
    return true;
    }
    else {
    return false;
    }
}
let arrayByCourse500 = courses.find(findByCoursePROJ500);
let courseStartByCourse500 = courses.find(findByCourseId).Title;

console.log(arrayByCourse500);
console.log(courseStartByCourse500);

// What are the titles of the courses that cost $50 or less?
console.log(" What are the titles of the courses that cost $50 or less?");

function filterByCourseUnder50(arrayValue) {
    if (arrayValue.Fee <=  50.00) {
    return true;
    }
    else {
    return false;
    }
}
let arrayByUnder50 = courses.filter(filterByCourseUnder50);
// let courseTitleByUnder50 = courses.filter(filterByCourseUnder50).Title;

for(let i = 0; i < arrayByUnder50.length; i++) {
    console.log("---");
    console.log(arrayByUnder50[i].Title);

}

// What classes meet in "Classroom 1"?
console.log("What classes meet in \"Classroom 1?\"");

function filterByClassroom1(arrayValue) {
    if (arrayValue.Location ==  "Classroom 1") {
    return true;
    }
    else {
    return false;
    }
}
let arrayByClassroom1 = courses.filter(filterByClassroom1);
// let courseTitleByUnder50 = courses.filter(filterByCourseUnder50).Title;

for(let i = 0; i < arrayByClassroom1.length; i++) {
    console.log("---");
    console.log(arrayByClassroom1[i].Title);

}

console.log("What classes meet in \"Classroom 7?\"");

let arrayByClassroom7 = courses.filter(array => array.Location == "Classroom 7");
for(let i = 0; i < arrayByClassroom7.length; i++) {
    console.log("---");
    console.log(arrayByClassroom7[i].Title);

}


// function log (word) {
//     console.log(word);
// }
let f2 = w => console.log(w);
console.dir(f2);