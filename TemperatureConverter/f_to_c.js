let deg_in_far = 70;
let deg_in_cel;

deg_in_cel = (deg_in_far - 32) * (5 / 9);

console.log(deg_in_cel);


function convertFtoC(deg_in_far) {
    let deg_in_cel = (deg_in_far - 32) * (5 / 9);
    return deg_in_cel;
}

function displayConvertFtoC(deg_in_far) {
    console.log("Temp in fahrenheit: " + deg_in_far);
    celsiusTemp = convertFtoC(deg_in_far);
    console.log("Temp in celsius   : " + celsiusTemp);
}


console.log("-----------------------------");
let currentTemp = 92;
let celsiusTemp = convertFtoC(currentTemp);
console.log(celsiusTemp);


console.log("-----------------------------");
let tempArray = [212, 90, 72, 32, 0, -40];
for (let i = 0; i < tempArray.length; i++) {
    console.log("Temp in fahrenheit: " + tempArray[i]);
    celsiusTemp = convertFtoC(tempArray[i]);
    console.log("Temp in celsius   : " + celsiusTemp);
}
console.log("----------------------------")
tempArray.forEach(displayConvertFtoC);