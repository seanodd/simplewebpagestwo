"use strict";


let employees = [
    {
        id: "55007",
        name: "Karina Chambers",
        jobTitle: "Tech Lead",
        yearsAtCompany: 4,
        email: "karinachambers@ziore.com",
        wfhAddress: "640 Boynton Place, Faxon, Kentucky, 42071",
        skillSet: "Velit commodo voluptate id est. Fugiat magna enim quis exercitation duis fugiat non nisi consequat.",
        projectsAssignedTo: [
            {
                projectId: 112,
                name: "Cupidatat aute"
            }
        ]
    },
    {
        id: "23810",
        name: "Kasey Bowers",
        jobTitle: "Senior Programmer",
        yearsAtCompany: 9,
        email: "kaseybowers@ziore.com",
        wfhAddress: "969 Clarendon Road, Marshall, Vermont, 47859",
        skillSet: "Est et voluptate incididunt deserunt culpa excepteur.",
        projectsAssignedTo: [
            {
                projectId: 124,
                name: "Amet do deserunttate aliqua"
            }
        ]
    },
    {
        id: "52797",
        name: "Bonner Church",
        jobTitle: "Senior Programmer",
        yearsAtCompany: 6,
        email: "bonnerchurch@ziore.com",
        wfhAddress: "158 Overbaugh Place, Bayview, Georgia, 31316",
        skillSet: "Qui minim minim aute anim reprehenderit nulla voluptate nulla.",
        projectsAssignedTo: [
            {
                projectId: 131,
                name: "Ut minim anim"
            },
            {
                projectId: 147,
                name: "Velit Lorem reprehenderit"
            }
        ]
    },
    {
        id: "62847",
        name: "Dejesus Fischer",
        jobTitle: "Backend Programmer",
        yearsAtCompany: 3,
        email: "dejesusfischer@ziore.com",
        wfhAddress: "228 Williams Place, Rote, Virgin Islands, 00804",
        skillSet: "Laborum incididunt ex adipisicing elit ullamco.",
        projectsAssignedTo: [
            {
                projectId: 131,
                name: "Ut minim anim"
            },
            {
                projectId: 133,
                name: "Est minim labore"
            },
            {
                projectId: 152,
                name: "Reprehenderit officia"
            }
        ]
    },
    {
        id: "16642",
        name: "Henderson Barber",
        jobTitle: "Senior Programmer",
        yearsAtCompany: 13,
        email: "hendersonbarber@ziore.com",
        wfhAddress: "805 Johnson Street, Ripley, Nevada, 89019",
        skillSet: "Nisi reprehenderit eiusmod cupidatat tempor laborum.",
        projectsAssignedTo: [
            {
                projectId: 131,
                name: "Ut minim anim"
            },
            {
                projectId: 123,
                name: "Reprehenderit reprehenderit"
            },
            {
                projectId: 105,
                name: "Pariatur eiusmod"
            }
        ]
    },
    {
        id: "32775",
        name: "Glenn Peterson",
        jobTitle: "Tech Lead",
        yearsAtCompany: 16,
        email: "glennpeterson@ziore.com",
        wfhAddress: "135 Russell Street, Cecilia, South Carolina, 29407",
        skillSet: "Commodo excepteur sit.",
        projectsAssignedTo: [
            {
                projectId: 134,
                name: "Dolore exercitation veniam"
            },
            {
                projectId: 131,
                name: "Ut minim anim"
            },
            {
                projectId: 107,
                name: "Ea sint irure do excepteur"
            }
        ]
    },
    {
        id: "76394",
        name: "Mcguire Alston",
        jobTitle: "Web Designer",
        yearsAtCompany: 2,
        email: "mcguirealston@ziore.com",
        wfhAddress: "245 Henry Street, Roosevelt, Minnesota, 56673",
        skillSet: "Esse sint occaecat ad aliqua sunt in eiusmod do.",
        projectsAssignedTo: [
            {
                projectId: 131,
                name: "Ut minim anim"
            },
            {
                projectId: 132,
                name: "Fugiat amet do cillum do"
            },
            {
                projectId: 142,
                name: "Sunt consequat incididunt"
            }
        ]
    },
    {
        id: "58889",
        name: "Lacey Mccarthy",
        jobTitle: "Web Programmer",
        yearsAtCompany: 1,
        email: "laceymccarthy@ziore.com",
        wfhAddress: "123 Kenilworth Place, Thynedale, Rhode Island, 02860",
        skillSet: "Dolore in mollit eu nulla sint ipsum nisi.",
        projectsAssignedTo: [
            {
                projectId: 113,
                name: "Magna aute sint"
            }
        ]
    },
    {
        id: "56701",
        name: "Benjamin Ferrell",
        jobTitle: "Backend Programmer",
        yearsAtCompany: 6,
        email: "benjaminferrell@ziore.com",
        wfhAddress: "929 Tehama Street, Belmont, Maine, 04952",
        skillSet: "Consequat laborum consectetur velit velit laboris eu tempor laborum elit occaecat irure.",
        projectsAssignedTo: [
            {
                projectId: 101,
                name: "Commodo exercitation"
            },
            {
                projectId: 131,
                name: "Mollit magna ad culpa"
            }
        ]
    },
    {
        id: "80897",
        name: "Hester Stout",
        jobTitle: "Senior Programmer",
        yearsAtCompany: 2,
        email: "hesterstout@ziore.com",
        wfhAddress: "816 Kings Place, Kempton, North Carolina, 27834",
        skillSet: "Do officia deserunt nulla.",
        projectsAssignedTo: [
            {
                projectId: 131,
                name: "Ut minim anim"
            }
        ]
    },
    {
        id: "49534",
        name: "Agnes Cooley",
        jobTitle: "Senior Programmer",
        yearsAtCompany: 21,
        email: "agnescooley@ziore.com",
        wfhAddress: "712 Louisa Street, Cherokee, Illinois, 60015",
        skillSet: "Anim sit dolore ullamco ut aliqua.",
        projectsAssignedTo: [
            {
                projectId: 150,
                name: "Adipisicing ea"
            },
            {
                projectId: 123,
                name: "Reprehenderit reprehenderit"
            }
        ]
    },
    {
        id: "33459",
        name: "Kerr Lynch",
        jobTitle: "Tech Lead",
        yearsAtCompany: 4,
        email: "kerrlynch@ziore.com",
        wfhAddress: "694 Albee Square, Fivepointville, Washington, 17517",
        skillSet: "Duis ex do ullamco sunt aliqua culpa aute.",
        projectsAssignedTo: [
            {
                projectId: 144,
                name: "Commodo anim occaecat"
            }
        ]
    },
    {
        id: "72054",
        name: "Kendra Perry",
        jobTitle: "Web Designer",
        yearsAtCompany: 1,
        email: "kendraperry@ziore.com",
        wfhAddress: "942 Conduit Boulevard, Alamo, Texas, 78516",
        skillSet: "Cupidatat amet qui proident ut aute eiusmod ex.",
        projectsAssignedTo: [
            {
                projectId: 107,
                name: "Ea sint irure do excepteur"
            },
            {
                projectId: 112,
                name: "Consequat non fugiat"
            },
            {
                projectId: 113,
                name: "Magna aute sint"
            }
        ]
    }
];

window.onload = init;
function init() {

    const employeeList = document.getElementById("employeeList");

    let selectHeader = new Option("Select a employee", "");

    employeeList.appendChild(selectHeader);
    for (let i = 0; i < employees.length; i++) {
        let theOption = new Option(employees[i].name, employees[i].id);
        employeeList.appendChild(theOption);
    }

    employeeList.onchange = onEmployeeListChanged;

}

function onEmployeeListChanged() {

    loadEmployeeTable(employeeList.selectedIndex);
    loadEmployeeDetailTable(employeeList.selectedIndex);
}

function loadEmployeeDetailTable(employeeIndex) {

    let employeeDetailTbl = document.getElementById("employeeDetailTbl");
    let employeeDetailTblBody = document.getElementById("employeeDetailTblBody");
    let employeeDetailProjectsTbl = document.getElementById("employeeDetailProjectsTbl");
    let employeeDetailProjectsTblBody = document.getElementById("employeeDetailProjectsTblBody");

    employeeDetailProjectsTblBody.innerHTML = "";
    employeeDetailTblBody.innerHTML = "";

    if (employeeIndex > 0) {
        employeeDetailTbl.style.display = 'block';
        buildEmployeeDetailRow(employeeDetailTblBody, employees[employeeIndex - 1]);
    } else {
        employeeDetailTbl.style.display = 'none';
        employeeDetailProjectsTbl.style.display = 'none';
    }

}

function buildEmployeeDetailRow(employeeDetailTblBody, employee) {
    // Create an empty <tr> element and add it to the last
    // position of the table
    for (const property in employee) {
        // console.log(`${property}: ${employee[property]}`);
        let value = employee[property];
        let rowName = property;
        if (property == "projectsAssignedTo") {
            value = employee[property].length;
            rowName = property;
            //continue;
        }

        let row = employeeDetailTblBody.insertRow(-1);
        // Create new cells (<td> elements) and add text
        let cell1 = row.insertCell(0);
        cell1.innerHTML = rowName;
        let cell2 = row.insertCell(1);
        cell2.innerHTML = value;

    }
    if (employee["projectsAssignedTo"].length > 0) {
        loadEmployeeDetailProjectsTable(employee["projectsAssignedTo"]);
    }

}

function loadEmployeeDetailProjectsTable(projects) {


    let employeeDetailProjectsTbl = document.getElementById("employeeDetailProjectsTbl");
    // loop through the array

    let employeeDetailProjectsTblBody = document.getElementById("employeeDetailProjectsTblBody");
    employeeDetailProjectsTblBody.innerHTML = "";
    if (projects.length > 0) {

        employeeDetailProjectsTbl.style.display = 'block';

        for (let i = 0; i < projects.length; i++) {
            buildProjectRow(employeeDetailProjectsTblBody, projects[i]);
        }
    } else {
        employeeDetailProjectsTbl.style.display = 'none';
    }
}

function buildProjectRow(tbody, project) {
    // Create an empty <tr> element and add it to the last
    // position of the table
    let row = tbody.insertRow(-1);
    // Create new cells (<td> elements) and add text
    let cell1 = row.insertCell(0);
    cell1.innerHTML = project.projectId;
    let cell2 = row.insertCell(1);
    cell2.innerHTML = project.name;

}

function loadEmployeeTable(employeeIndex) {
    //console.log("employeeIndex: " + employeeIndex);
    // Find the table
    let tbody = document.getElementById("employeeTbl");
    // loop through the array

    let employeeTblBody = document.getElementById("employeeTblBody");
    employeeTblBody.innerHTML = "";
    if (employeeIndex > 0) {
        tbody.style.display = 'block';
        buildEmployeeRow(employeeTblBody, employees[employeeIndex - 1]);
    } else {
        tbody.style.display = 'none';
    }

    //  alert(table.rows.length);
}

function buildEmployeeRow(tbody, employee) {
    // Create an empty <tr> element and add it to the last
    // position of the table
    let row = tbody.insertRow(-1);
    // Create new cells (<td> elements) and add text
    let cell1 = row.insertCell(0);
    cell1.innerHTML = employee.id;
    let cell2 = row.insertCell(1);
    cell2.innerHTML = employee.name;
    let cell3 = row.insertCell(2);
    cell3.innerHTML = employee.jobTitle;
    let cell4 = row.insertCell(3);
    cell4.innerHTML = employee.yearsAtCompany;
    let cell5 = row.insertCell(4);
    cell5.innerHTML = employee.email;
}


