
function printContact(location) {
    console.log(location.name);
    console.log(location.address);
    console.log(location.city + ", " + location.state + " " + location.zip);
    }

let locationOne = {
    name: "Pursalane Faye",
    address: "121 Main Street",
    city: "Benbrook",
    state: "Texas",
    zip: "76126"
    };
printContact(locationOne);