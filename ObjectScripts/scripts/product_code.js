function parsePartCode(code) {
    
    let supplierCode = code.substring(0, code.indexOf(":"));
    let productNumber = code.substring(code.indexOf(":") + 1, code.indexOf("-"));
    let size = code.substring(code.indexOf("-") + 1);

    let part = {
        supplierCode: supplierCode,
        productNumber: productNumber,
        size: size
    };

return part;

}



let partCode1 = "XYZ:1234-L";
let part1 = parsePartCode(partCode1);

console.log("Supplier: " + part1.supplierCode +
" Product Number: " + part1.productNumber +
" Size: " + part1.size);