
function add(a, b) {
    return a + b;
}

function print(word) {
    console.log(word);
}

// add equivalent
let f1 = (a, b) => a + b;
console.log(f1(2, 4));

// print
let f2 = (s1, s2) => console.log(s1, s2);
f2("JavaScript", "fun");

function find(arr, f) {
    for (let i = 0; i < arr.length; i++) {
        if (f(arr[i])) {
            return arr[i];
        }
    }
}

console.log("--- Greet ---");
function greet() {
    console.log("Hi!");
}

let gArrow = () => console.log("Hi!");
greet();
gArrow();

console.log("--- square ---");
function square(x) {
    return x * x;
}
let squareArrow = (x) => Math.pow(x, 2);
console.log("square     : " + square(4));
console.log("squareArrow: " + squareArrow(4));

console.log("--- isPositive ---");
function isPositive(x) {
    return x >= 0;
}

let isPositiveArrow = (x) => x >= 0;
console.log("isPositive     : " + isPositive(4));
console.log("isPositiveArrow: " + isPositiveArrow(4));
console.log("isPositive     : " + isPositive(-4));
console.log("isPositiveArrow: " + isPositiveArrow(-4));

console.log("--- subtract ---");
function subtract(x, y) {
    return x - y;
}

let subtractArrow = (x, y) => x - y;

console.log("subtract     : " + subtract(4, 3));
console.log("subtractArrow: " + subtractArrow(4, 3));


console.log("--- biggestOfTwo ---");
function biggestOfTwo(x, y) {
    if (x > y) {
        return x;
    } else if (x < y) {
        return y;
    }
}

let biggestOfTwoArrow = (x, y) => {
    if (x > y) {
        return x;
    } else if (x < y) {
        return y;
    }
}
console.log("biggestOfTwo     : " + biggestOfTwo(45, 31));
console.log("biggestOfTwoArrow: " + biggestOfTwoArrow(45, 31));


console.log("--- findBiggest ---");
function findBiggest(arr) {
    let biggest = arr[0];
    for (let i = 1; i < arr.length; i++) {
        if (biggest < arr[i]) {
            biggest = arr[i];
        }
    }
    return biggest;
}

let findBiggestArrow = (arr) => {
    let biggest = arr[0];
    for (let i = 1; i < arr.length; i++) {
        if (biggest < arr[i]) {
            biggest = arr[i];
        }
    }
    return biggest;
}
console.log("findBiggest     : " + findBiggest([1, 3, 5, 99, 2, 5, 1000, 4]));
console.log("findBiggestArrow: " + findBiggestArrow([1,3,5,99,2,5,1000,4]));
