"use strict";
window.onload = init;
function init() {

    const allDivs = document.getElementsByTagName("div");
    const allPs = document.getElementsByTagName("p");
    for (let i = 0; i < allDivs.length; i++) {
        allDivs[i].style.border = "1px solid black";
    }

    // for (let i = 0; i < allDivs.length; i++) {
    //   console.dir(allPs[i]);
    // }

    let imgAltDescriptions = ["Ziggy", "Orangie", "Two Cats"];

    const imgElements = document.querySelectorAll("img");
    for (var i = 0; i < imgElements.length; i++) {
        imgElements[i].setAttribute('alt', imgAltDescriptions[i]);
        imgElements[i].className = "roundedImg";
        allPs[i].innerHTML = imgAltDescriptions[i];
    };




}
function display(item) {
    console.log(item);
}