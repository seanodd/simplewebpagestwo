"use strict";
window.onload = init;
function init() {
    const greetBtn = document.getElementById("greetBtn");
    greetBtn.onclick = onGreetBtnClicked;
}
function onGreetBtnClicked() {
    let nameField = document.getElementById("nameField");
    alert("Hi there " + nameField.value + "!");
}