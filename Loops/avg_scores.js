let myScores = [92, 98, 84, 76, 89, 99, 100];
let yourScores = [82, 98, 94, 88, 92, 100];


console.log("myScores Average  : " + getAverage(myScores));
console.log("yourScores Average: " + getAverage(yourScores));

console.log("myScores Median  : " + getMedian(myScores));
console.log("yourScores Median: " + getMedian(yourScores));

function getAverage(allScores) {
    let totalScore = 0;
    let average = 0;
    for (let i = 0; i < allScores.length; i++) {
        totalScore = totalScore + allScores[i];
    }
    average = totalScore / allScores.length;
    return average;
}

function getMedian(allScores) {

    allScores.sort(compareAscendingNumber);

    if (allScores.length % 2 != 0) {
        let middleIndex = Math.floor(allScores.length / 2)
        return allScores[middleIndex];
    } else {

        let middleIndex = Math.floor(allScores.length / 2);
        return (allScores[middleIndex] + allScores[middleIndex-1])/2;
    }
}

function compareAscendingNumber(a, b) {
    // if a is smaller, a-b is negative so don't swap!
    return a - b;
}

console.dir