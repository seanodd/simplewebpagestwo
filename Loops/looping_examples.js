console.log("------ FOR -----");

for (let i = 0; i < 7; i++) {
    console.log("I love loops");
}

console.log("------ WHILE -----");

let i = 0;
while (i < 7) {
    console.log("I love loops");
    i++;
}    