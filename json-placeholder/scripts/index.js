"use strict";
window.onload = init;
function init() {

    const getToDoBtn = document.getElementById("getToDoBtn");
    const getUsersBtn = document.getElementById("getUsersBtn");

    if (getToDoBtn) {
        getToDoBtn.onclick = onGetToDoBtnClicked;
    }
    if (getUsersBtn) {
        getUsersBtn.onclick = onGetUsersBtnClicked;
    }

}
function onGetToDoBtnClicked() {
    let toDoField = document.getElementById("toDoField");
    if (toDoField.value > 0 && toDoField.value < 201) {
        let element = document.getElementById("messageArea");
        fetch("http://jsonplaceholder.typicode.com/todos/" + toDoField.value)
            .then(response => response.json())
            .then(data => {
                let message = "ToDo - Title: " + data.title;
                //element.innerHTML = message;
                buildToDoTable(data);
            });
    }
}

function buildToDoTable(data) {
    // Create an empty <tr> element and add it to the last
    // position of the table
    let toDoTblBody = document.getElementById("toDoTblBody");
    let toDoTbl = document.getElementById("toDoTbl");
    toDoTblBody.innerHTML = "";
    toDoTbl.style.display = 'block';

    console.log(data);
    //{userId: 1, id: 2, title: 'quis ut nam facilis et officia qui', completed: false}
    const labels = {
        userId: 'User ID',
        id: 'ToDo ID',
        title: 'Title',
        completed: 'Completed'
    };

    const replacedKeysInData = {}
    Object.keys(data).forEach((key) => {
        const keyFromMap = labels[key] || key;
        replacedKeysInData[keyFromMap] = data[key]
    })

    data = replacedKeysInData;

    console.log(data);
    // {User ID: 1, ToDo ID: 2, Title: 'quis ut nam facilis et officia qui', Completed: false}

    let renameKeys = (keysMap, object) =>
    Object.keys(object).reduce(
      (acc, key) => ({
        ...acc,
        ...{ [keysMap[key] || key]: object[key] },
      }),
      {}
    );
  
  let result = renameKeys(labels, 
    data);
  console.log(result);
  //{User ID: 1, ToDo ID: 2, Title: 'quis ut nam facilis et officia qui', Completed: false}

    for (const property in data) {
        //console.log(`${property}: ${data[property]}`);
        let value = data[property];
        let rowName = property;
        if (property == "projectsAssignedTo") {
            value = data[property].length;
            rowName = property;
            //continue;
        }

        let row = toDoTblBody.insertRow(-1);
        // Create new cells (<td> elements) and add text
        let cell1 = row.insertCell(0);
        cell1.innerHTML = rowName;
        let cell2 = row.insertCell(1);
        cell2.innerHTML = value;

    }

}

function onGetUsersBtnClicked() {

    let table = document.getElementById("usersTbl");
    let tableBody = document.getElementById("usersTblBody");
    tableBody.innerHTML = "";
    table.style.display = 'block';
    fetch("http://jsonplaceholder.typicode.com/users")
        .then(response => response.json())
        .then(data => {
            for (let i = 0; i < data.length; i++) {
                let row = tableBody.insertRow(-1);
                let cell1 = row.insertCell(0);
                let cell2 = row.insertCell(1);
                let cell3 = row.insertCell(2);
                let cell4 = row.insertCell(3);
                let cell5 = row.insertCell(4);
                let cell6 = row.insertCell(5);
                cell1.innerHTML = data[i].id;
                cell2.innerHTML = data[i].name;
                cell3.innerHTML = data[i].email;
                cell4.innerHTML = data[i].username;
                cell5.innerHTML = data[i].phone;
                cell6.innerHTML = data[i].website;

            }
        });

}
