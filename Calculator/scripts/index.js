"use strict";
window.onload = init;
function init() {
    const addBtn = document.getElementById("addBtn");
    const subtractBtn = document.getElementById("subtractBtn");
    const multiplyBtn = document.getElementById("multiplyBtn");
    const divideBtn = document.getElementById("divideBtn");
    addBtn.onclick = onAddBtnClicked;
    subtractBtn.onclick = onSubtractBtnClicked;
    multiplyBtn.onclick = onMultiplyBtnClicked;
    divideBtn.onclick = onDivideBtnClicked;

}
function onAddBtnClicked() {
    let number1Field = document.getElementById("number1Field");
    let number2Field = document.getElementById("number2Field");
    let answerField = document.getElementById("answerField");

    answerField.value = Number(number1Field.value) + Number(number2Field.value);
}

function onSubtractBtnClicked() {
    let number1Field = document.getElementById("number1Field");
    let number2Field = document.getElementById("number2Field");
    let answerField = document.getElementById("answerField");

    answerField.value = Number(number1Field.value) - Number(number2Field.value);
}

function onMultiplyBtnClicked() {
    let number1Field = document.getElementById("number1Field");
    let number2Field = document.getElementById("number2Field");
    let answerField = document.getElementById("answerField");

    answerField.value = Number(number1Field.value) * Number(number2Field.value);
}

function onDivideBtnClicked() {
    let number1Field = document.getElementById("number1Field");
    let number2Field = document.getElementById("number2Field");
    let answerField = document.getElementById("answerField");

    answerField.value = Number(number1Field.value) / Number(number2Field.value);
}